#!/usr/bin/env node
'use strict';

const requireDir = require('require-dir');
const utils = requireDir('utils');

const config = utils.file.readJson('config.json');

const IRCD = require('./modules/ircd/ircd');

const ircd = new IRCD(config.ircd);
