'use strict';

class Server {
	#sid;
	#capabilities = [];
	#name;
	#hopcount;
	#description;
	#users = {};
	
	constructor(sid) {
		this.#sid = sid;
	}
	
	get sid() {
		return this.#sid;
	}
	
	get capabilities() {
		return this.#capabilities;
	}
	
	set capabilities(capabilities) {
		this.#capabilities = capabilities;
	}
	
	get name() {
		return this.#name;
	}
	
	set name(name) {
		this.#name = name;
	}
	
	get hopcount() {
		return this.#hopcount;
	}
	
	set hopcount(hopcount) {
		this.#hopcount = hopcount;
	}
	
	get description() {
		return this.#description;
	}
	
	set description(description) {
		this.#description = description;
	}
	
	addUser(user) {
		if (this.#users.hasOwnProperty(user.uid)) {
			console.log('User ' + user.uid + ' already exists!');
			return;
		}
		
		this.#users[user.uid] = user;
	}
}

module.exports = Server;
