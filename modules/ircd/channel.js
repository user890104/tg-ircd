'use strict';

class Channel {
	#name;
	#ts;
	#modes = [];
	#users = [];
	#maskLists = {
		b: [],
		e: [],
		I: []
	};
	
	constructor(name, ts, modes) {
		this.#name = name;
		this.#ts = ts;
		this.#modes = modes;
	}
	
	get name() {
		return this.#name;
	}
	
	get ts() {
		return this.#ts;
	}
	
	set ts(ts) {
		this.#ts = ts;
	}
	
	get modes() {
		return this.#modes;
	}
	
	set modes(modes) {
		this.#modes = modes;
	}
	
	hasMode(mode) {
		return Object.keys(this.#modes).indexOf(mode) > -1;
	}
	
	getMode(mode) {
		if (!this.hasMode(mode)) {
			return false;
		}
		
		return this.#modes[mode];
	}
	
	addMode(mode, param) {
		this.#modes[mode] = param === undefined ? true : param;
	}
	
	removeMode(mode) {
		if (!this.hasMode(mode)) {
			return;
		}
		
		delete this.#modes[mode];
	}
	
	get key() {
		return this.getMode('k');
	}
	
	set key(key) {
		if (key === undefined) {
			this.removeMode('k');
		}
		else {
			this.addMode('k', key);
		}
	}
	
	get limit() {
		return this.getMode('l');
	}
	
	set limit(limit) {
		if (limit === undefined) {
			this.removeMode('l');
		}
		else {
			this.addMode('l', limit);
		}
	}
	
	get inviteOnly() {
		return this.getMode('l');
	}
	
	set inviteOnly(flag) {
		if (flag) {
			this.addMode('i');
		}
		else {
			this.removeMode('i');
		}
	}
	
	get moderated() {
		return this.getMode('m');
	}
	
	set moderated(flag) {
		if (flag) {
			this.addMode('m');
		}
		else {
			this.removeMode('m');
		}
	}
	
	get noExtMsgs() {
		return this.getMode('n');
	}
	
	set noExtMsgs(flag) {
		if (flag) {
			this.addMode('n');
		}
		else {
			this.removeMode('n');
		}
	}
	
	get private() {
		return this.getMode('p');
	}
	
	set private(flag) {
		if (flag) {
			this.addMode('p');
		}
		else {
			this.removeMode('p');
		}
	}
	
	get secret() {
		return this.getMode('s');
	}
	
	set secret(flag) {
		if (flag) {
			this.addMode('s');
		}
		else {
			this.removeMode('s');
		}
	}
	
	get opsTopic() {
		return this.getMode('t');
	}
	
	set opsTopic(flag) {
		if (flag) {
			this.addMode('t');
		}
		else {
			this.removeMode('t');
		}
	}
	
	get sslOnly() {
		return this.getMode('S');
	}
	
	set sslOnly(flag) {
		if (flag) {
			this.addMode('S');
		}
		else {
			this.removeMode('S');
		}
	}
	
	get userCount() {
		return Object.keys(this.#users).length;
	}

	addUser(user) {
		this.#users[user.uid] = user;
	}
	
	addUsers(users) {
		this.#users = {
			...this.#users,
			...users
		};
	}
	
	removeUser(uid) {
		delete this.#users[uid];
	}

	get bans() {
		return this.#maskLists.b;
	}

	get exempts() {
		return this.#maskLists.e;
	}

	get inviteExempts() {
		return this.#maskLists.I;
	}

	addToMaskList(list, mask) {
		const listArr = this.#maskLists[list];

		if (listArr.indexOf(mask) > -1) {
			return;
		}

		listArr.push(mask);
	}

	removeFromMaskList(list, mask) {
		const listArr = this.#maskLists[list];
		const idx = listArr.indexOf(mask);

		if (idx === -1) {
			return;
		}

		listArr.splice(idx, 1);
	}

	addBan(mask) {
		this.addToMaskList('b', mask);
	}

	addExempt(mask) {
		this.addToMaskList('e', mask);
	}

	addInviteExempt(mask) {
		this.addToMaskList('I', mask);
	}

	removeBan(mask) {
		this.removeFromMaskList('b', mask);
	}

	removeExempt(mask) {
		this.removeFromMaskList('e', mask);
	}

	removeInviteExempt(mask) {
		this.removeFromMaskList('I', mask);
	}
}

module.exports = Channel;
