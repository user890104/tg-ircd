'use strict';

class Message {
	#raw;
	#tags;
	#prefix;
	#command;
	#params;
	
	constructor(message) {
		this.#raw = message.raw;
		this.#tags = message.tags;
		this.#prefix = message.prefix;
		this.#command = message.command;
		this.#params = message.params;
	}

	get raw() {
		return this.#raw;
	}

	get tags() {
		return this.#tags;
	}
	
	get prefix() {
		return this.#prefix;
	}
	
	get command() {
		return this.#command;
	}
	
	get params() {
		return this.#params;
	}
}

module.exports = Message;
