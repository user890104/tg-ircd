'use strict';

const EventEmitter = require('events');
const net = require('net');
const ircMessage = require('irc-message');

class Socket extends EventEmitter {
	#socket;
	#ircMessageSocket;

	constructor(connectConfig) {
		super();
		this.#socket = net.createConnection(connectConfig, () => {
			console.log('Connected');
			this.emit('connected');
		}).on('end', () => {
			console.log('Connection closed');
			this.emit('end');
		});
		this.#ircMessageSocket = ircMessage.createStream().on('data', message => {
			console.log('->', JSON.stringify(message.raw));
			this.emit('message', message);
		});
		this.#socket.pipe(this.#ircMessageSocket);
	}
	
	sendCmd(command) {
		console.log('<-', JSON.stringify(command));
		this.#socket.write(command + '\r\n');
	}
	
	error(msg) {
		this.#socket.removeAllListeners('end');
		this.#ircMessageSocket.removeAllListeners('data');
		this.sendCmd('ERROR :' + msg);
		this.#socket.end();
	}
}

module.exports = Socket;
