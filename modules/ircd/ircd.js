'use strict';

const Socket = require('./socket');

class IRCD {
	#config;
	#socket;
	#protocol;
	#servers = {};
	#users = {};
	#channels = {};
	#currConnSid;
	
	constructor(config) {
		this.#config = config;
		this.#socket = new Socket(config.connect);
		this.#protocol = require('./protocol')(this);
		this.#socket.on('message', message => {
			this.#protocol.handleMessage(message);
		}).on('connected', () => {
			this.#protocol.introduce();
		});
	}
	
	error(msg) {
		this.#socket.error(msg);
	}
	
	get config() {
		return this.#config;
	}
	
	sendCmd(data) {
		this.#socket.sendCmd(data);
	}
	
	hasServer(sid) {
		return this.#servers.hasOwnProperty(sid);
	}
	
	addServer(server) {
		if (this.hasServer(server.sid)) {
			console.warn('Server ' + server.sid + ' already exists!');
			return;
		}
		
		this.#servers[server.sid] = server;
	}
	
	getServer(sid) {
		if (!this.hasServer(sid)) {
			return null;
		}
		
		return this.#servers[sid];
	}
	
	getCurrConnServer() {
		if (!this.#currConnSid) {
			console.warn('Not currently connecting to a server!');
			return null;
		}
		
		return this.getServer(this.#currConnSid);
	}
	
	set currConnSid(currConnSid) {
		this.#currConnSid = currConnSid;
	}
	
	hasUser(uid) {
		return this.#users.hasOwnProperty(uid);
	}
	
	getUser(uid) {
		if (!this.hasUser(uid)) {
			return null;
		}
		
		return this.#users[uid];
	}
	
	addUser(user) {
		if (this.hasUser(user.uid)) {
			console.warn('User ' + user.uid + ' already exists!');
			return;
		}
		
		this.#users[user.uid] = user;
		user.server.addUser(user);
	}
	
	hasChannel(name) {
		return this.#channels.hasOwnProperty(name);
	}
	
	getChannel(name) {
		if (!this.hasChannel(name)) {
			return null;
		}
		
		return this.#channels[name];
	}
	
	addChannel(channel) {
		if (this.hasChannel(channel.name)) {
			console.warn('Channel ' + channel.name + ' already exists!');
			return;
		}
		
		this.#channels[channel.name] = channel;
	}
	
	deleteChannel(channel) {
		if (!this.hasChannel(channel.name)) {
			console.warn('Channel ' + channel.name + ' not found!');
			return;
		}
		
		delete this.#channels[channel.name];
	}
}

module.exports = IRCD;
