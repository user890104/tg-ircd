'use strict';

const Message = require('./message');
const Server = require('./server');
const User = require('./user');
const Channel = require('./channel');

class ProtocolError extends Error {}

module.exports = function(ircd) {
	const TS_VERSION = 6;
	
	let handlers = {};
	
	function getTs() {
		return Math.floor(Date.now() / 1000);
	}

	function handleMessage(message) {
		const msg = new Message(message);

		if (handlers.hasOwnProperty(msg.command)) {
			try {
				handlers[msg.command](msg);
			}
			catch (ex) {
				if (ex instanceof ProtocolError) {
					ircd.error(ex.message);
				}
				else {
					throw ex;
				}
			}
		}
		else {
			console.log(msg.prefix, msg.command, msg.params);
		}
	}
	
	function parseModeChanges(modes, modeChangeStr) {
		let operation = '+';
		let characters = modeChangeStr.split('');
		
		while (characters.length > 0) {
			const character = characters.shift();
			
			if (character === '+' || character === '-') {
				operation = character;
				continue;
			}
			
			const idx = modes.indexOf(character);
			
			if (operation === '+' && idx === -1) {
				modes.push(character);
			}
			
			if (operation === '-' && idx !== -1) {
				modes.splice(idx, 1);
			}
		}
		
		return modes;
	}
	
	function parseJoinList(list) {
		let users = {};
		
		list.forEach(user => {
			const uid = user.slice(-9);
			users[uid] = user.slice(-11, -9).split('');
		});
		
		return users;
	}

	function getCurrConnServer(errCommand) {
		const server = ircd.getCurrConnServer();

		if (!server) {
			throw new ProtocolError('Received ' + errCommand + ' outside of introduction');
		}

		return server;
	}

	function getServer(sid) {
		const server = ircd.getServer(sid);

		if (!server) {
			throw new ProtocolError('Server ' + sid + ' not found!');
		}

		return server;
	}

	function getUser(uid) {
		const user = ircd.getUser(uid);

		if (!user) {
			throw new ProtocolError('User ' + uid + ' not found!');
		}

		return user;
	}

	function getChannel(name) {
		const channel = ircd.getChannel(name);

		if (!channel) {
			throw new ProtocolError('Channel ' + name + ' not found!');
		}

		return channel;
	}

	function introduce() {
		const linkConfig = ircd.config.link.local;
		const ts = getTs();
		const capabilities = ['QS', 'EX', 'IE'];

		ircd.sendCmd('PASS ' + linkConfig.password + ' TS ' + TS_VERSION + ' :' + linkConfig.sid);
		ircd.sendCmd('CAPAB :' + capabilities.join(' '));
		ircd.sendCmd('SERVER ' + linkConfig.name + ' 1 :' + linkConfig.description);
		ircd.sendCmd('SVINFO ' + TS_VERSION + ' ' + TS_VERSION + ' 0 ' + ts);
	}

	handlers['PING'] = msg => {
		ircd.sendCmd('PONG :' + msg.params[0]);
	};
	
	handlers['NOTICE'] = msg => {
		console.log('[' + msg.params[0] + ']', msg.params[1]);
	};
	
	handlers['PASS'] = msg => {
		const password = msg.params[0];
		const protocol = msg.params[1];
		const protocolVersion = parseInt(msg.params[2]);
		const sid = msg.params[3];
		
		if (password !== ircd.config.link.remote.password) {
			throw new ProtocolError('Invalid password');
		}
		
		if (protocol !== 'TS') {
			throw new ProtocolError('Invalid protocol');
		}
		
		if (protocolVersion !== TS_VERSION) {
			throw new ProtocolError('Invalid protocol version');
		}
		
		if (sid === ircd.config.link.local.sid) {
			throw new ProtocolError('SID already exists');
		}
		
		const server = new Server(sid);
		ircd.currConnSid = sid;
		ircd.addServer(server);
	};
	
	handlers['CAPAB'] = msg => {
		const server = getCurrConnServer('CAPAB');
		server.capabilities = msg.params[0].split(' ');
	};

	handlers['SERVER'] = msg => {
		const server = getCurrConnServer('SERVER');
		const name = msg.params[0];
		const hopcount = parseInt(msg.params[1]);
		const description = msg.params[2];
		
		if (name !== ircd.config.link.remote.name) {
			throw new ProtocolError('Invalid server name');
		}
		
		if (hopcount !== 1) {
			throw new ProtocolError('Invalid hopcount');
		}
		
		server.name = name;
		server.hopcount = hopcount;
		server.description = description;
	};

	handlers['SVINFO'] = msg => {
		// noinspection JSUnusedLocalSymbols
		const server = getCurrConnServer('SVINFO');
		const currentTsVersion = parseInt(msg.params[0]);
		//const minTsVersion = parseInt(msg.params[1]);
		//const unused = parseInt(msg.params[2]);
		const remoteTs = parseInt(msg.params[3]);
		
		if (currentTsVersion !== TS_VERSION) {
			throw new ProtocolError('Invalid TS version');
		}
		
		const localTs = getTs();
		const deltaTs = localTs - remoteTs;
		console.log('TS delta=', deltaTs);
		
		const deltaMax = ircd.config.link.settings.delta_max;
		
		if (Math.abs(deltaTs) > deltaMax) {
			throw new ProtocolError('TS delta too high, got ' + deltaTs + ', expected <=' + deltaMax);
		}
		
		// end of introduction
		ircd.currConnSid = null;
	};
	
	handlers['UID'] = msg => {
		const server = getServer(msg.prefix);
		const nickname = msg.params[0];
		const hopcount = parseInt(msg.params[1]);
		const ts = parseInt(msg.params[2]);
		const umodes = parseModeChanges([], msg.params[3]);
		const username = msg.params[4];
		const hostname = msg.params[5];
		const ip = msg.params[6];
		const uid = msg.params[7];
		const gecos = msg.params[8];

		const user = new User(uid, server, ts, hopcount, username, ip, gecos);
		user.nickname = nickname;
		user.umodes = umodes;
		user.hostname = hostname;
		
		ircd.addUser(user);
	};
	
	handlers['MODE'] = msg => {
		const user = getUser(msg.params[0]);
		user.umodes = parseModeChanges(user.umodes, msg.params[1]);
	};
	
	handlers['SJOIN'] = msg => {
		const ts = parseInt(msg.params[0]);
		const name = msg.params[1];
		const modesAndModeParams = msg.params.slice(2, -1);
		const users = parseJoinList(msg.params[msg.params.length - 1].split(' '));

		const channelModes = parseModeChanges([], modesAndModeParams.shift());
		let modes = {};

		channelModes.forEach(mode => {
			if (['k', 'l'].indexOf(mode) > -1) {
				const modeParam = modesAndModeParams.shift();
				modes[mode] = mode === 'l' ? parseInt(modeParam) : modeParam;
			}
			else {
				modes[mode] = true;
			}
		});

		let channel = ircd.getChannel(name);
		
		if (channel) {
			if (ts < channel.ts) {
				channel.ts = ts;
				channel.modes = modes;
			}
		}
		else {
			channel = new Channel(name, ts, modes);
			ircd.addChannel(channel);
		}
		
		channel.addUsers(users);
	};
	
	handlers['PART'] = msg => {
		const channel = getChannel(msg.params[0]);
		const reason = msg.params[1] || null;
		
		channel.removeUser(msg.prefix, reason);
		
		if (channel.userCount === 0) {
			ircd.deleteChannel(channel);
		}
	};

	handlers['JOIN'] = msg => {
		const user = getUser(msg.prefix);
		const ts = parseInt(msg.params[0]);
		const name = msg.params[1];
		//const plusSign = msg.params[2];

		let channel = ircd.getChannel(name);

		if (channel) {
			if (ts < channel.ts) {
				channel.ts = ts;
				channel.modes = [];
			}
		}
		else {
			channel = new Channel(name, ts, []);
			ircd.addChannel(channel);
		}

		channel.addUser(user);
	};

	handlers['TMODE'] = msg => {
		const ts = parseInt(msg.params[0]);
		const channel = getChannel(msg.params[1]);
		const modes = msg.params[2];
		const modeParams = msg.params.slice(3);


		let operation = '+';
		let characters = modes.split('');

		while (characters.length > 0) {
			const character = characters.shift();

			if (character === '+' || character === '-') {
				operation = character;
				continue;
			}

			let param;

			if (
				['k', 'b', 'e', 'I'].indexOf(character) > -1 ||
				(character === 'l' && operation === '+')
			) {
				param = modeParams.shift();
			}

			if (['b', 'e', 'I'].indexOf(character) > -1) {
				if (operation === '+') {
					channel.addToMaskList(character, param);
				}

				if (operation === '-') {
					channel.removeFromMaskList(character, param);
				}
			}
			else {
				if (operation === '+') {
					channel.addMode(character, param);
				}

				if (operation === '-') {
					channel.removeMode(character);
				}
			}
		}
	};

	handlers['KICK'] = msg => {
		const channel = getChannel(msg.params[0]);
		const user = getUser(msg.params[1]);
		//const reason = msg.params[2];

		channel.removeUser(user.uid);
	};

	return {
		introduce,
		handleMessage
	};
};
